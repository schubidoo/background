<?php

$backgroundExists = FALSE;
$backgroundPath = getConfigValue('background_path');

if(!empty($backgroundPath)) {
    $backgroundExists = TRUE;
}

$backgroundFade= json_decode(getConfigValue('background_backgroundFade'));
if(empty($backgroundFade) || $backgroundFade=='""') {
    $backgroundFade = '0.5';
}

$output = ($backgroundExists ? "url('$backgroundPath')" : "");
?>

<script>
	window.addEventListener('DOMContentLoaded', loadBG, false);

//linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url("/modules/background/assets/background.jpg")
	function loadBG () 
	{
			$(document.body).css("background","linear-gradient(rgba(0,0,0,<?php echo $backgroundFade; ?>), rgba(0,0,0,<?php echo $backgroundFade; ?>)), <?php echo $output; ?>");
	        //$(document.body).css("background-image", <?php echo $output; ?>);
			//$(document.body).css("background-size", "cover");
	}
</script>
