��    
      l      �       �      �   2     0   B     s     �     �     �     �     �  �  �  #   �  2   �  0   �     /  
   O     Z     a     f     l              
                    	           No logo set. Upload an image. Please choose a valid image file (JPEG, PNG, GIF). Please choose an image file from your hard disk. branding_description branding_title center left right save Project-Id-Version: mirr.OS module Branding
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-02-13 16:54+0100
PO-Revision-Date: 2018-03-18 18:39+0100
Last-Translator: Tobias Grasse <mail@tobias-grasse.net>
Language-Team: Tobias Grasse <tg@glancr.de>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ../../..
X-Poedit-SearchPath-0: .
 No background set. Upload an image. Please choose a valid image file (JPEG, PNG, GIF). Please choose an image file from your hard disk. Add extra omph! to your Glancr. Background center left right save 