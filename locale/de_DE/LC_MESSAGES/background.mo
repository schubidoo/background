��    
      l      �       �   #   �   2     0   H     y     �     �     �     �     �  �  �  6   �  7   �  3     .   S  
   �     �     �     �  	   �               
             	                 No background set. Upload an image. Please choose a valid image file (JPEG, PNG, GIF). Please choose an image file from your hard disk. background_description background_title center left right save Project-Id-Version: mirr.OS Modul Branding
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-03-18 18:38+0100
PO-Revision-Date: 2018-03-18 18:40+0100
Last-Translator: Tobias Grasse <mail@tobias-grasse.net>
Language-Team: Tobias Grasse <tg@glancr.de>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ../../..
X-Poedit-SearchPath-0: .
 Kein Background ausgewählt. Bitte ein Bild hochladen. Bitte eine gültige Bilddatei wählen (JPEG, PNG, GIF). Bitte eine Bilddatei von der Festplatte auswählen. Gib Deinem Glancr eine graphisches „Umpf“! Background mittig linksbündig rechtsbündig speichern 