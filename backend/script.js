/**
 * Backend UI logic for the background/logo modal.
 */

if (!String.prototype.includes) {
    String.prototype.includes = function(search, start) {
        'use strict';
        if (typeof start !== 'number') {
            start = 0;
        }

        if (start + search.length > this.length) {
            return false;
        } else {
            return this.indexOf(search, start) !== -1;
        }
    };
}

var errorContainer = $('#backgroundFileError');

// Trigger logo file deletion when the "remove" button is clicked.
$('#background__preview').on('click', '#background__remove', function() {

    var previewBox = $('#background__preview');
    $.post('../modules/background/assets/deleteBackground.php')
        .done(function() {
            $.post('setConfigValueAjax.php', {'key' : 'background_path', 'value' : ''});
            previewBox.empty().append("<p>" + BACKGROUND_LOCALE.noImagePath + "</p><img src='' alt='' />");
        })
        .fail(function () {
            previewBox.append("<p class='error'>" + BACKGROUND_LOCALE.deletionError + "</p>");
        });
});


// Uoload new logo images & handle preview.
$('#background__edit').click(function() {

    var backgroundFileInput = $('#background_image'),
        uploadButton = $('#background__edit'),
        file = backgroundFileInput[0].files[0];

    if(backgroundFileInput.val() === '') {
        errorContainer.text(BACKGROUND_LOCALE.notAnImage).animate({
            opacity: 0
        }, 3000, function() {
            errorContainer.text('');
            errorContainer.css('opacity',1);
        });

        return;
    }

    // Test if the chosen file has a valid image MIME type.
    if(!file.type.includes('image/')) {

        // Clear the input.
        backgroundFileInput.val('');

        //@TODO: localize error message
        $('#backgroundFileError').text(BACKGROUND_LOCALE.invalidMimeType).animate({
            opacity: 0
        }, 3000, function() {
            errorContainer.text('');
            errorContainer.css('opacity',1);
        });

        return;

    }

    // Code below gets executed if the above tests pass.

    // Handle UI upload state: Disabled button, loading spinner.
    uploadButton.attr("disabled", true);
    uploadButton.addClass('loading__button');
    var initialText = uploadButton.text();
    uploadButton.html('<div class="loading__button--inner"></div>');

    var fd = new FormData();
    fd.append("file", file);
    $.ajax({
        url: backgroundFileInput.parent('form').attr('action'),
        type: "POST",
        data: fd,
        processData: false,
        contentType: false,
        success: function(response) {

            // If the file upload was successful, save the path in config and display a preview.
            var imagePath = JSON.parse(response).imagePath;
            $.post('setConfigValueAjax.php', {'key' : 'background_path', 'value' : imagePath});
            $('#background__preview').empty().append(
                "<p>Aktuell gewähltes Hintergrundbild:</p>" +
                "<img src=\"" + imagePath + " \" alt=\"Background\" />" +
                "<button id='background__remove'>Entfernen</button>"
            );

            $('#ok').show(30, function() {
                $(this).hide('slow');
            })
        },
        error: function (response) {
            errorContainer.text(response.body);
            $('#error').show(30, function() {
                $(this).hide('slow');
            })
        },
        complete: function () {
            backgroundFileInput.val('');
            uploadButton.attr("disabled", false);
            uploadButton.html(initialText);
            uploadButton.removeClass('loading__button');
        }
    })
});