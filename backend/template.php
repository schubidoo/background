<?php

// Make sure these strings are available in the translation files, since they're partly derived from variables at runtime.
_('background_title');
_('background_description');

_('left');
_('center');
_('right');

$backgroundExists = FALSE;

$backgroundPath = getConfigValue('background_path');
if(!empty($backgroundPath)) {
    $backgroundExists = TRUE;
}

$backgroundFade= json_decode(getConfigValue('background_backgroundFade'));
if(empty($backgroundFade) || $backgroundFade=='""') {
    $backgroundFade = '0.0';
}

?>

<p>Lade hier dein background als Bilddatei (JPEG, PNG, GIF) hoch, um es auf Deinem glancr anzuzeigen!</p>

<div id="background__preview">
<?php print($backgroundExists ?
    "<p>Aktuell gewähltes background:</p><img src=\"$backgroundPath\" alt=\"background\"><button id='background__remove' type='button'>Entfernen</button>"
    :
    "<p>". _('No background set. Upload an image.') ."</p><img src=\"\" alt='' />");
?>
</div>



<h5>Neuen Hintergrund hochladen</h5>
<form action="../modules/background/assets/uploadBackground.php">
    <input type="file" id="background_image" name="backgroundImage" accept="image/*" class="">
</form>

<div id="backgroundFileError"></div>

<div class="block__add" id="background__edit">
	<button class="background__edit--button" href="#">
		<span><?php echo _('save'); ?></span>
	</button>
</div>

<br>
<p>Hintergrund abdunkeln</p>
<select id="backgroundFade">
  <option value="0.0">0%</option>
  <option value="0.25">25%</option>
  <option value="0.5">50%</option>
  <option value="0.75">75%</option>
</select>

<script type="text/javascript">
    var background_LOCALE = {
        "noImagePath": "<?php echo _('No background set. Upload an image.') ?>",
        "invalidMimeType": "<?php echo _('Please choose a valid image file (JPEG, PNG, GIF).') ?>",
        "notAnImage": "<?php echo _('Please choose an image file from your hard disk.') ?>"
    }
	
	document.getElementById('backgroundFade').value="<?php echo $backgroundFade ?>";
	
	
	$('#backgroundFade').change(function() {
	    var backgroundFade = $("#backgroundFade").val();
		//alert(JSON.stringify(thumbnailStyle));
	    $.post('/config/setConfigValueAjax.php', {'key' : 'background_backgroundFade', 'value': JSON.stringify(backgroundFade)}).done(function() {
							                        $('#ok').show(30, function() {
							                                $(this).hide('slow');
							                        });
												});
		});	
</script>