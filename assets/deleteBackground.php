<?php
/**
 * Delete a logo file from the module's asset folder.
 */

require_once '../../../config/glancrConfig.php';
$existingBackgroundPath = getConfigValue('background_path');

// Delete the logo file upon request.
if(!empty($existingBackgroundPath)) {
    $result = unlink(GLANCR_ROOT . $existingBackgroundPath);
    if ($result) {
        http_response_code('200');
    } else {
        http_response_code('500');
    }
    return;
}