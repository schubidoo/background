<?php
include('../../../config/glancrConfig.php');

$backgroundPath = getConfigValue('background_path');
if(empty($backgroundPath)) {
	$backbgroundPath = '';
}

echo json_encode($backgroundPath);
